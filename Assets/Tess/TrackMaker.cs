using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrackMaker : MonoBehaviour
{
    public Shader drawShader;
    private Material drawMaterial;
    private Material snowMaterial;
    private RenderTexture splatmap;
    public GameObject _terrain;

    private Vector3 pos;

    [Range(0, 5)]
    public float _brushSize;
    [Range(0, 1)]
    public float _brushStrength;

    RaycastHit groundHit;
    int layerMask;

    private Vector3 scale = Vector3.one;
    public float snowScale;
    public float snowDepth;
    private List<Vector2> vectors = new List<Vector2>();

    private void Awake()
    {
        _terrain = GameObject.Find("Plane");
    }
    void Start()
    {
        layerMask = LayerMask.GetMask("Terrain");
        drawMaterial = new Material(drawShader);
        snowMaterial = _terrain.GetComponent<MeshRenderer>().material;
        snowMaterial.SetTexture("_Splat", splatmap = new RenderTexture(1024, 1024, 0, RenderTextureFormat.ARGBFloat));

        //snowDepth = snowMaterial.GetFloat("_Displacement");     
    }

    void Update()
    {
        pos = transform.position;

        if (Physics.Raycast(pos, Vector3.down, out groundHit, 10, layerMask))
        {
            Vector2 newHitCoord = groundHit.textureCoord;
            newHitCoord.x = Mathf.Round(newHitCoord.x * 100) / 100;
            newHitCoord.y = Mathf.Round(newHitCoord.y * 100) / 100;

            if (!vectors.Contains(newHitCoord))
            {
                scale += (snowScale*Vector3.one);
                _brushSize += snowScale;
                transform.localScale = scale;
                vectors.Add(newHitCoord);
            }

            drawMaterial.SetVector("_Coordinate", new Vector4(newHitCoord.x, newHitCoord.y));
            drawMaterial.SetFloat("_Strength", _brushStrength);
            drawMaterial.SetFloat("_Size", _brushSize);
            RenderTexture temp = RenderTexture.GetTemporary(splatmap.width, splatmap.height, 0, RenderTextureFormat.ARGBFloat);
            Graphics.Blit(splatmap, temp);
            Graphics.Blit(temp, splatmap, drawMaterial);
            RenderTexture.ReleaseTemporary(temp);
        }
    }
}
