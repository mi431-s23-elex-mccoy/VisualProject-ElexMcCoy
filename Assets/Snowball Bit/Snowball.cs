using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Snowball : MonoBehaviour
{
    public float snowScale;

    private ParticleSystem part;

    private Texture2D tex;
    private Material mat;
    private Renderer rend;
    private RenderTexture rendTex;
    private RenderTexture rendTexRef;

    private Vector3 pos;
    private Vector3 scale;
    private Vector3 down;

    //private bool firstBall = true;
    public uint trailStore;

    [SerializeField]
    private LayerMask layerMask;

    void Start()
    {
        trailStore = 0;
        rendTexRef = rendTex;
        scale = transform.localScale;
        //down = transform.TransformDirection(Vector3.down);
    }

    void Update()
    {
        //rendTex.IncrementUpdateCount();

        /*if (trailStore != rendTex.updateCount)
        {
            Snowballing();
            trailStore = rendTex.updateCount;
            rendTexRef = rendTex;
        }*/


        pos = transform.position;

        RaycastHit hit;

        if (Physics.Raycast(pos, down, out hit, Mathf.Infinity, layerMask))
        {
            part = hit.transform.GetComponent<ParticleSystem>();

            if (part != null)
            {
                Debug.Log("Snowball is not above fresh snow!");
                return;
            }
            else
            {
                Snowballing();
            }

            // Below was trying to read the Texture itself, which wasnt possible
            /*rend = hit.transform.GetComponent<Renderer>();
            MeshCollider meshColl = hit.collider as MeshCollider;

             if (rend == null || meshColl == null) return;

            //Vector2 pixelUV = hit.textureCoord;
            mat = rend.material;
            if (mat == null) return;
            //tex = mat.mainTexture as Texture2D;
            //Shader shader = mat.shader;
            //shader.Tex

            if (tex == null) return;
            //if (mat.HasTexture("TrailMap")) return;
            //Texture2D trail = mat.GetTexture("TrailMap") as Texture2D;

            //if (firstBall == true)
            //{
            //    trailStore = 0;
            //    firstBall = false;
            //}

            trailStore = rendTex.updateCount;*/

            // The below works rn when i move the snowball above the terrain
            // The issue seemed to be that the MeshRenderer is actually above 
            // if (pixelUV.y < pos.y) Snowballing();
        }
        else
        {
            Debug.Log("Snowball cannot see particles");
        }

        //Snowballing();
    }

    public void Snowballing()
    {
        scale += (snowScale * Vector3.one);
        transform.localScale = scale;
    }
}
