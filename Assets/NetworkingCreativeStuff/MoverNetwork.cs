using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FishNet.Connection;
using FishNet.Object;

//[RequireComponent(typeof(Rigidbody))]
public class MoverNetwork : NetworkBehaviour
{
    public float speed = 10;

    Rigidbody rigid;

    private Camera playerCam;
    private float cameraYOffset = 0.5f;

    private void Awake()
    {
        rigid = GetComponent<Rigidbody>();
    }

    public override void OnStartClient()
    {
        base.OnStartClient();
        if (base.IsOwner)
        {
            playerCam = Camera.main;
        }
        else
        {
            gameObject.GetComponent<MoverNetwork>().enabled = false;
        }
    }

    void FixedUpdate()
    {
        float h = Input.GetAxis("Horizontal");
        float v = Input.GetAxis("Vertical");

        Vector3 vel = new Vector3(h, 0, v);
        vel *= speed;
        rigid.velocity = vel;
    }
}
