using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FishNet.Connection;
using FishNet.Object;

public class SnowballManager : NetworkBehaviour
{
    public List<TrackNetwork> trackMakers;

    public Shader drawShader;
    public Material drawMat;
    private Material snowMaterial;
    private RenderTexture splatmap;
    private GameObject _terrain;


    public void NewBaller(TrackNetwork tracker)
    {
        trackMakers.Add(tracker);
    }

    public void BallerGone(TrackNetwork tracker)
    {
        trackMakers.Remove(tracker);
    }

    public void Start()
    {
        /*if (!base.IsServer)
        {
            gameObject.GetComponent<SnowballManager>().enabled = false;
        }*/

        _terrain = GameObject.Find("Plane");
        drawMat = new Material(drawShader);

        snowMaterial = _terrain.GetComponent<MeshRenderer>().material;
        snowMaterial.SetTexture("_Splat", splatmap = new RenderTexture(1024, 1024, 0, RenderTextureFormat.ARGBFloat));
    }

    /*public void Update()
    {
        if (trackMakers.Count < 2) return;
        for (int i = 0; i < trackMakers.Count; i ++)
        {
            for (int j = 1; j < trackMakers.Count; j++)
            {
                TrackNetwork trackI = trackMakers[i];
                TrackNetwork trackJ = trackMakers[j];
                Graphics.Blit(trackI.splatmap, trackJ.splatmap, trackJ.drawMaterial);
                Graphics.Blit(trackJ.splatmap, trackI.splatmap, trackI.drawMaterial);
            }
        }
    }*/

    public void DrawTracks(Vector2 newHitCoord, float _brushStrength, float _brushSize)
    {
        drawMat.SetVector("_Coordinate", new Vector4(newHitCoord.x, newHitCoord.y));
        drawMat.SetFloat("_Strength", _brushStrength);
        drawMat.SetFloat("_Size", _brushSize);
        RenderTexture temp = RenderTexture.GetTemporary(splatmap.width, splatmap.height, 0, RenderTextureFormat.ARGBFloat);
        Graphics.Blit(splatmap, temp);
        Graphics.Blit(temp, splatmap, drawMat);
        RenderTexture.ReleaseTemporary(temp);
    }
}
